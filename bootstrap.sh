#!/usr/bin/env bash

printf '%s\n%s\n' '# Zend Server' 'deb http://repos.zend.com/zend-server/8.5/deb_apache2.4 server non-free' > /etc/apt/sources.list.d/zend.list
wget http://repos.zend.com/zend.key -O- | apt-key add -

wget https://repo.percona.com/apt/percona-release_0.1-3.$(lsb_release -sc)_all.deb
dpkg -i percona-release_0.1-3.$(lsb_release -sc)_all.deb
rm -f percona-release_0.1-3.$(lsb_release -sc)_all.deb
export DEBIAN_FRONTEND=noninteractive

aptitude update
aptitude full-upgrade
aptitude install -y git percona-server-server-5.7 sendmail zend-server-php-5.6

echo 'export PATH=$PATH:/usr/local/zend/bin' >> /etc/profile.d/zend-server.sh
echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/zend/lib' >> /etc/profile.d/zend-server.sh

chmod o+x /var/log/apache2
chmod o+r /var/log/apache2/{access,error}.log

if ! [ -L /var/www/html ]; then
  rm -rf /var/www/html
  ln -fs /vagrant/public /var/www/html
fi

php -r "readfile('https://getcomposer.org/installer');" > composer-setup.php
php -r "if (hash('SHA384', file_get_contents('composer-setup.php')) === '41e71d86b40f28e771d4bb662b997f79625196afcca95a5abf44391188c695c6c1456e16154c75a211d238cc3bc5cb47') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
mv composer.phar /usr/local/bin/composer
